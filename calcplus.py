#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo
import calcoohija


file=open(sys.argv[1] + '.txt','r')
lines=file.readlines()
file.close()

for line in lines:

	print ("")

	chain = line.split(',')
	length = len(chain)
	i=2

	operation = chain[0]
	result = float(chain[1])

	while i <= length - 1:


		if i>0:
			if operation == "suma":
				result = calcoo.Calculadora.suma(result,float(chain[i]))

			if operation == "resta":
				result = calcoo.Calculadora.resta(result,float(chain[i]))

			if operation == "multiplica":
				result = calcoohija.CalculadoraHija.multiplica(result,float(chain[i]))

			if operation == "divide":
				result = calcoohija.CalculadoraHija.divide(result,float(chain[i]))

		i = i+1

	print(operation + ':')
	print(result)

print("")


