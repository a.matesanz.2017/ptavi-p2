#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo

class CalculadoraHija():

    def multiplica(op1, op2):
        """ This multiplies the operators """
        return op1 * op2

    def divide(op1, op2):
        """ This this divides the operators """
        if op2 == 0:
            sys.exit("Division by zero is not allowed")
        return op1 / op2

if __name__ == '__main__':

	print("")
	print(sys.argv)

	#por si el usuario se equivoca
	if len(sys.argv) != 4:
		sys.exit("Too much arguments")

	_, op1, oper, op2 = sys.argv

	dic = {
		"suma": calcoo.Calculadora.suma,
		"resta": calcoo.Calculadora.resta,
		"multiplica": CalculadoraHija.multiplica,
		"divide": CalculadoraHija.divide,
		}

	if oper not in dic:
		sys.exit("Unsupported operation")

	try:
		op1 = float(op1)
		op2 = float(op2)

	except ValueError:
		sys.exit(" Non numerical parameters")

	result = dic[oper](float(sys.argv[1]), float(sys.argv[3]))
	print(result)
	print("")
