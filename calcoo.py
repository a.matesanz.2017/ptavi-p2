#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

class Calculadora():
    """My class"""

    def suma(op1, op2):
        """ This adds the operators """
        return op1 + op2

    def resta(op1, op2):
        """ This subtracts the operators """
        return op1 - op2

if __name__ == "__main__":

	print("")
	print(sys.argv)

	#por si el usuario se equivoca
	if len(sys.argv) != 4:
		sys.exit("Too much arguments")

	_, op1, oper, op2 = sys.argv

	dic = {
		"suma": Calculadora.suma,
		"resta": Calculadora.resta,
		}

	if oper not in dic:
		sys.exit("Unsupported operation")

	try:
		op1 = float(op1)
		op2 = float(op2)

	except ValueError:
		sys.exit(" Non numerical parameters")

	result = dic[oper](float(sys.argv[1]), float(sys.argv[3]))
	print(result)	
	print("")



